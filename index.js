const express = require('express')
const app = express()
const port = 3000

var path = require('path')

app.use(express.static('public'))

app.post('/', (req, res) => {
  res.sendFile(path.resolve('./succes.html'))
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})